This project implements a basic REST server to model a video rental store.

The framework uses DropWizard as recommended as well as hibernate for the data.

# Use

The project was developed using the IntelliJ IDE and the main class is `src/main/java/App.java`.

The generated jar file is quite large so I've placed the jar file on Dropbox and Google drive:
https://dl.dropboxusercontent.com/u/1896301/casumo-1.jar
https://drive.google.com/file/d/0B3w_pZCai4ydMk4zTUxGd2lyWk0/view?usp=sharing

In order to run the jar file, first extract the casumo.zip file into a folder and download the far file into the folder.
Using the command line you can run the server by issuing the following command:
```
$ java -jar casumo-1.jar server config.xml
```

You may then send http requests to `localhost` on port `8080`.

All requests use url-encoded form data and all responses are in JSON similar to a typical REST API.


# Design and Implementation

In any system it's important to separate the components so they take up less space and are easier to modify.
The application and REST API were broken up into the following components.
- `customers`  
- `films`
- `inventory` 
- `rentals` 
- `prices`

**Source Code**

Since the DropWizard/Jersey/Hibernate framework was used I tried to more or less keep aligned to how they prefer you do it. 
So the code was sectioned into the following packages:
- `resources` which are the REST handlers/controllers which parse the request, perform the business logic and respond.
- `data` which represents the model in the form of Hibernate objects and is kept separate from the controllers.
- `dao` which is the DropWizard/Hibernate way of retrieving the data objects.

There were also the `calculators` and `results` packages which represent contain calculators the price/bonus points/overdue
calculators and REST responses respectively.

The contents of these components very much reflect the *customer*/*film*/*rental*/*inventory* design of the system.

**REST**

As the Jersey framework was used the REST requests were represented in a standard http format where the GET/POST/.. are
the verbs and the path the resource. Also using url encoded input formats made the implementation in Jersey quite clean.
As JSON is a very common format it was used for all responses.

The following verb resource endpoints exist:

    POST    /customers              
    GET     /customers/{id}         
    DELETE  /customers/{id}/rentals 
    GET     /customers/{id}/rentals 
    POST    /customers/{id}/rentals 
    POST    /films 
    POST    /inventory 
    GET     /inventory/{id} 
    GET     /prices 
    GET     /rentals 
    POST    /rentals 


## Customers

In order to represent the customers it is a good idea to make it it's own entity and record properties such as 
`name` and **bonus** `points`.

The REST API also has convenience end-points to rent a film easily which automatically adds bonus point to the customer.

**Create Customer** 

All we need to create a customer is the name. Note that for this API call you can only create one customer at a time.
Below is the general format of the request:

`POST /customers` & `name:<String>` -> `{id:<Integer>, name:<String>, points:<Integer>}`

Example:
```
$ curl -X POST -H "Accept:application/json" \
    -d "name=Killian" \
    http://localhost:8080/application/customers

{"id":1,"name":"Killian","points":0}
```

**Get Customer**:

`GET /customers/{id}` -> `{id:<Integer>, name:<String>, points:<Integer>}`

Example:
```
$ curl -X GET -H "Accept:application/json" http://localhost:8080/application/customers/1

{"id":1,"name":"killian","points":0}
```

## Film

Each film needs to be stored with information such as `title` and `enum` film `type`. This requires it's own table,
model and REST endpoints.

**Create Film**

Note that in this call you can use it to create more than one film per request.

`POST /films` & `[title:<String> type:NEW|OLD|REGULAR]` -> `{items: {id:<Integer>, title:<String>}`

Example:
```
$ curl -X POST -H "Accept:application/json" \
   -d "title=New+Movie" -d "type=NEW" \
   -d "title=Reg+Movie" -d "type=REGULAR" \
   -d "title=Old+Movie" -d "type=OLD" \
   http://localhost:8080/application/films

{"items":[
    {"id":1,"title":"New Movie","type":"NEW"},
    {"id":2,"title":"Reg Movie","type":"REGULAR"},
    {"id":3,"title":"Old Movie","type":"OLD"}],
 "count":3}
```

## Inventory

Since it's a rental library there can be many physical copies of the same film. So it's a good idea to have an `inventory`
component in which every film may have multiple entries. Currently we only really need the `film_id` property but
in a real world we'd record other information such as time created, condition, etc..

**Create Inventory**

All we need is a film to create an item and you can can create multiple items per film.

`POST /inventory` & `[film_id:<Integer>]` -> `{items:[{id:<Integer> film_id:<Integer>}]}`

Example:
```
$ curl -X POST -H "Accept:application/json" \
    -d "film_id=1" \
    -d "film_id=2" \
    -d "film_id=2" \
    -d "film_id=3" \
    http://localhost:8080/application/inventory

{"items":[
    {"id":1,"film_id":1},
    {"id":2,"film_id":2},
    {"id":3,"film_id":2},
    {"id":4,"film_id":3}],
 "count":4}
```

## Rental

It's necessary to keep track of the rentals which includes properties such as `start` and `due` times.

In this application the `rentals` only stores the inventory that's currently being rented out and once a rental is returned
it is deleted from the database. Hence the `inventory_id` is unique. In a more advanced system we would the `end` time would
be recorded and kept in the database after it has been returned. 

For this application this component isn't really necessary and could be integrated into `inventory`.

**Create Rental**

There are two methods to create a rental. The first method is by posting to `/rentals` however this a general purpose
method and does not increment the bonus points or calculate the price. So it is recommended to use the convenience
method in the `customers` API point in order to create a rental. 


`POST /customer/{id}/rentals` & `[inventory_id:<Integer> duration:<Integer>]` 
    -> ```{customer_id:<Integer> price:<Integer> points:<Integer> count:<Integer>
           items:[{inventory_id:<Integer> price:<Integer> points:<Integer> start:<Time> due:<Time> duration:<DurationDays>}}]}```

Example:
```
$ curl -X POST -H "Accept:application/json"  \
    -d "inventory_id=1" -d "duration=3" \
    -d "inventory_id=2" -d "duration=5" \
    -d "inventory_id=3" -d "duration=1" \
    http://localhost:8080/application/customers/1/rentals

{"customer_id":1, "price":200, "points":4, "count":3
 "items":[
    {"price":120,"points":2,"start":1483312837809,"due":1483572037809,"duration":3,"inventory_id":1},
    {"price":50,"points":1,"start":1483312837809,"due":1483744837809,"duration":5,"inventory_id":2},
    {"price":30,"points":1,"start":1483312837809,"due":1483399237809,"duration":1,"inventory_id":3}],}
````

**Get Customers Rentals**

It is desirable to be able to list the rentals for a customer.
`GET /customers/{id}/rentals` 
    -> ```{customer_id:<Integer> fine:<Integer> count:<Integer> num_overdue:<Integer>
            items:[{inventory_id:<Integer> start:<Time> due:<Time> now:<Time> duration:<DurationDays> overdue:<Integer> fine:<Integer>}]}```

Note that overdue is in days and that a negative number means how many days are remaining, thus a positive number means
an item is overdue.
There is some duplication in this as duration and overdue are not needed but it makes it more convenient.

Example:
```
$ curl -X GET -H "Accept:application/json" http://localhost:8080/application/customers/1/rentals
    
{"items":[
    {"start":1483312837809,"due":1483572037809,"now":1483312954921,"duration":3,"overdue":-2,"fine":0,"inventory_id":1},
    {"start":1483312837809,"due":1483399237809,"now":1483312954921,"duration":1,"overdue":0,"fine":0,"inventory_id":3},
    {"start":1483312837809,"due":1483744837809,"now":1483312954921,"duration":5,"overdue":-4,"fine":0,"inventory_id":2}],
 "fine":0, "count":3, "customer_id":1, "num_overdue":0}
```

**Find Customer from Inventory**

When a customer returns the rental they don't usually give their ID, so in order to get their id from an 
inventory item there exists an API call.

`GET /rentals` & `[inventory_id:<Integer>]` 
    -> `[{id:<Integer> inventory_id:<Integer> customer_id:<Integer> start:<Time> due:<Time>}]`

Note that the `id` is the rental id and is not used anywhere except internally.

Example:
```
$ curl -X GET -H "Accept:application/json" \
    "http://localhost:8080/application/rentals?inventory_id=1&inventory_id=2&inventory_id=3"
    
[{"id":4,"start":1483313203636,"due":1483572403636,"inventory_id":1,"customer_id":1},
 {"id":5,"start":1483313203636,"due":1483745203636,"inventory_id":2,"customer_id":1},
 {"id":6,"start":1483313203636,"due":1483399603636,"inventory_id":3,"customer_id":1}]
```

**Returning/Delete Rental**

Returning a rental is the equivalent to deleting a rental from the customers rentals resource. 
In a more advanced system you would also deduct the overdue fine from their account here.

The list of inventories was kept in the query parameter as Jersey (and some servers) don't allow a `DELETE` body.
Although deleting multiple items can be problematic because the entire request fails if one of the resource does not exist
and it's not really practical to determine which one from the error message.

`DELETE /customers/{id}/rentals` & `[inventory_id:<Integer>]` 
    -> `{"items":[{inventory_id:<Integer> start:<Integer> due:<Integer> end:<Integer> overdue:<Integer> fine:<Integer>}]`

Example:
```
$ curl -X DELETE -H "Accept:application/json" \
    "http://localhost:8080/application/customers/1/rentals?inventory_id=1&inventory_id=2&inventory_id=3"

{"fine":0, "count":3, "customer_id":1, "num_overdue":0,
 "items":[
    {"start":1483312837809,"due":1483572037809,"end":1483313060366,"duration":3,"overdue":-2,"fine":0,"inventory_id":1},
    {"start":1483312837809,"due":1483744837809,"end":1483313060366,"duration":5,"overdue":-4,"fine":0,"inventory_id":2},
    {"start":1483312837809,"due":1483399237809,"end":1483313060366,"duration":1,"overdue":0,"fine":0,"inventory_id":3}]}
```

## Prices

Used to calculate the price for a rentals as well as overdue items.

`GET /prices` & `[inventory_id:<Integer>] duration:<Time> type:OVERDUE|RENTAL`
 -> `{price:<Integer> points:<Integer>
        items:[{inventory_id:<Integer> price:<Integer> points:<Integer>}]}`

```
$ curl -X GET -H "Accept:application/json" \
    "http://localhost:8080/application/prices?inventory_id=1&inventory_id=2&duration=3&type=RENTAL"
    
{"price":0,"points":3,"count":2
 "items":[
    {"price":120,"points":2,"inventory_id":0},
    {"price":30,"points":1,"inventory_id":0}]}
```

# Comments

The body of the requests could possibly use JSON as it's more flexible and has types.

The pricing system is not advanced. The bonus points are hard coded in and the prices are stored in the prices table
which just stores the `premium` and `basic` prices. Some sort of rule based matching system could possibly be created
in order to match a price to a product.
I also couldn't seem to get the price values shown in the examples given.
 
There could also be a lot more tests.