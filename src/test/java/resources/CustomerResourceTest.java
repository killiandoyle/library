package resources;

import calculators.BonusPointsCalculator;
import calculators.OverdueFineCalculator;
import calculators.RentalPriceCalculator;
import dao.*;
import data.*;
import data.Currency;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import results.CustomerRentalCreateResult;
import results.CustomerRentalDeleteResult;
import results.CustomerRentalListResult;
import results.CustomerResult;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;


public class CustomerResourceTest {
    private static final RentalDAO RENTAL_DAO = mock(RentalDAO.class);
    private static final InventoryDAO INVENTORY_DAO = mock(InventoryDAO.class);
    private static final FilmDAO FILM_DAO = mock(FilmDAO.class);
    private static final CustomerDAO CUSTOMER_DAO = mock(CustomerDAO.class);
    private static final PriceDAO PRICE_DAO = mock(PriceDAO.class);

    @ClassRule
    public static final ResourceTestRule RESOURCES = ResourceTestRule.builder()
            .addResource(new CustomerResource(CUSTOMER_DAO, RENTAL_DAO, INVENTORY_DAO, PRICE_DAO))
            .build();

    Film filmOld, filmReg, filmNew;
    Inventory inventoryOld1, inventoryOld2, inventoryReg, inventoryNew;
    Customer customer;

    final long basicPrice = 30;
    final long premiumPrice = 40;

    @Before
    public void setUp() {
        Price basic = new Price(Price.Type.BASIC, basicPrice, Currency.SEK);
        Price premium = new Price(Price.Type.PREMIUM, premiumPrice, Currency.SEK);
        when(PRICE_DAO.findByType(eq(Price.Type.BASIC))).thenReturn(basic);
        when(PRICE_DAO.findByType(eq(Price.Type.PREMIUM))).thenReturn(premium);

        customer = new Customer(1, "Killian", 10);
        when(CUSTOMER_DAO.findById(eq(customer.getId()))).thenReturn(customer);

        filmOld = new Film(1,"Old movie", Film.Type.OLD);
        filmReg = new Film(2,"Reg movie", Film.Type.REGULAR);
        filmNew = new Film(3,"New movie", Film.Type.NEW);
        when(FILM_DAO.findById(eq(filmOld.getId()))).thenReturn(filmOld);
        when(FILM_DAO.findById(eq(filmReg.getId()))).thenReturn(filmReg);
        when(FILM_DAO.findById(eq(filmNew.getId()))).thenReturn(filmNew);

        inventoryOld1 = new Inventory(1, filmOld);
        inventoryOld2 = new Inventory(2, filmOld);
        inventoryReg = new Inventory(3, filmReg);
        inventoryNew = new Inventory(4, filmNew);
        when(INVENTORY_DAO.findById(eq(inventoryOld1.getId()))).thenReturn(inventoryOld1);
        when(INVENTORY_DAO.findById(eq(inventoryOld2.getId()))).thenReturn(inventoryOld2);
        when(INVENTORY_DAO.findById(eq(inventoryReg.getId()))).thenReturn(inventoryReg);
        when(INVENTORY_DAO.findById(eq(inventoryNew.getId()))).thenReturn(inventoryNew);
    }

    @After
    public void tearDown() {
        reset(RENTAL_DAO);
        reset(PRICE_DAO);
        reset(FILM_DAO);
        reset(INVENTORY_DAO);
    }

    final BonusPointsCalculator pointsCalculator = new BonusPointsCalculator();
    final RentalPriceCalculator priceCalculator = new RentalPriceCalculator(basicPrice, premiumPrice);
    final OverdueFineCalculator fineCalculator = new OverdueFineCalculator(basicPrice, premiumPrice);

    @Test
    public void create() {
        Customer newCustomer = new Customer(1, "Killian", 0);
        when(CUSTOMER_DAO.create(eq(newCustomer.getName()))).thenReturn(newCustomer);

        CustomerResult result = RESOURCES.client()
                .target("/customers")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new Form().param("name", newCustomer.getName()),
                        MediaType.APPLICATION_FORM_URLENCODED_TYPE),
                        CustomerResult.class);

        assertEquals(result.id, newCustomer.getId());
        assertEquals(result.name, newCustomer.getName());
        assertEquals(result.points, newCustomer.getPoints());
    }

    @Test
    public void get() {
        CustomerResult result = RESOURCES.client()
                .target("/customers/" + customer.getId())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(CustomerResult.class);

        assertEquals(customer.getId(), result.id);
        assertEquals(customer.getName(), result.name);
        assertEquals(customer.getPoints(), result.points);
    }

    @Test
    public void createRentals() {
        final DateTime now = new DateTime();
        class Case {
            String label;
            Film film;
            Inventory inventory;
            Rental rental;
            long price, points;

            Case(String label, Rental rental) {
                this.label = label;
                this.rental = rental;
                this.inventory = rental.getInventory();
                this.film = inventory.getFilm();
                this.price = priceCalculator.calculate(film.getType(), rental.getDuration());
                this.points =  pointsCalculator.calculate(film.getType(), rental.getDuration());
            }
        }
        List<Case> cases = new LinkedList<>();
        cases.add(new Case("old1", new Rental(1, customer, inventoryOld1, now, now.plusDays(1))));
        cases.add(new Case("old2", new Rental(2, customer, inventoryOld2, now, now.plusDays(2))));
        cases.add(new Case("reg", new Rental(3, customer, inventoryReg, now, now.plusDays(3))));
        cases.add(new Case("new", new Rental(4, customer, inventoryNew, now, now.plusDays(4))));

        for (Case c : cases) {
            when(RENTAL_DAO.create(eq(customer), eq(c.inventory), any(DateTime.class), any(DateTime.class)))
                    .thenReturn(c.rental);
        }

        long customerPoints = customer.getPoints();

        Form form = new Form().param("start", "" + now.getMillis());
        for (Case c : cases) {
            form.param("inventory_id", "" + c.inventory.getId())
                .param("duration", "" + c.rental.getDuration().getDays());
        }

        CustomerRentalCreateResult result = RESOURCES.client()
                .target("/customers/" + customer.getId() + "/rentals")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE),
                        CustomerRentalCreateResult.class);

        assertEquals(customer.getId(), result.customerId);
        assertEquals(cases.size(), result.getItems().size());

        long price = 0, points = 0;
        for (int i = 0; i < cases.size(); i++) {
            Case c = cases.get(i);
            CustomerRentalCreateResult.Item item = result.getItems().get(i);
            assertEquals(c.label, c.inventory.getId(), item.inventoryId);
            assertEquals(c.label, c.rental.getDuration().getDays(), item.duration);
            assertEquals(c.label, now.getMillis(), item.start.getMillis());
            assertEquals(c.label, c.rental.getDue().getMillis(), item.due.getMillis());
            assertEquals(c.label, c.points, item.points);
            assertEquals(c.label, c.price, item.price);

            price += c.price;
            points += c.points;
        }
        assertEquals(price, result.price);
        assertEquals(points, result.points);
        assertEquals(customerPoints + points, customer.getPoints());
    }

    @Test
    public void getRentals() {
        Customer customer = new Customer(2, "James", 0);

        final DateTime now = new DateTime();
        class Case {
            String label;
            Film film;
            Inventory inventory;
            Rental rental;
            long fine, points;

            Case(String label, Rental rental) {
                this.label = label;
                this.rental = rental;
                this.inventory = rental.getInventory();
                this.film = inventory.getFilm();
                this.fine = fineCalculator.calculate(film.getType(), rental.getOverdue(now));
            }
        }
        Map<Long, Case> cases = new HashMap();
        cases.put(inventoryOld1.getId(), new Case("old1", new Rental(1, customer, inventoryOld1, now.minusDays(2), now.plusDays(1))));
        cases.put(inventoryOld2.getId(), new Case("old2", new Rental(2, customer, inventoryOld2, now.minusDays(10), now.plusDays(3))));
        cases.put(inventoryNew.getId(), new Case("new", new Rental(3, customer, inventoryNew, now.minusDays(10), now.plusDays(3))));

        Set<Rental> rentals = new HashSet<>();
        for (Case c : cases.values()) rentals.add(c.rental);
        Customer mockCustomer = mock(Customer.class);
        when(CUSTOMER_DAO.findById(eq(customer.getId()))).thenReturn(mockCustomer);
        when(mockCustomer.getRentals()).thenReturn(rentals);
        when(mockCustomer.getId()).thenReturn(customer.getId());

        CustomerRentalListResult result = RESOURCES.client()
                .target("/customers/" + customer.getId() + "/rentals")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(CustomerRentalListResult.class);

        assertEquals(customer.getId(), result.customerId);
        assertEquals(cases.size(), result.getItems().size());
        int numOverdue = 0;
        long fine = 0;
        for (CustomerRentalListResult.Item item : result.getItems()) {
            assertTrue("" + item.inventoryId, cases.containsKey(item.inventoryId));
            Case c = cases.get(item.inventoryId);
            assertEquals(c.label, c.inventory.getId(), item.inventoryId);
            assertEquals(c.label, c.rental.getStart().getMillis(), item.start.getMillis());
            assertEquals(c.label, c.rental.getDue().getMillis(), item.due.getMillis());
            assertEquals(c.label, c.rental.getDuration().getDays(), item.duration);
            assertEquals(c.label, c.fine, item.fine);
            assertEquals(c.label, c.rental.getOverdue(item.now).getDays(), item.overdue);
            fine += c.fine;
            if (c.rental.getOverdue(item.now).getDays() > 0) numOverdue++;
        }
        assertEquals(numOverdue, result.numOverdue);
        assertEquals(fine, result.fine);
    }

    @Test
    public void returnRentals() {
        final DateTime now = new DateTime();
        class Case {
            String label;
            Film film;
            Inventory inventory;
            Rental rental;
            long fine;

            Case(String label, Rental rental) {
                this.label = label;
                this.rental = rental;
                this.inventory = rental.getInventory();
                this.film = inventory.getFilm();
                this.fine = fineCalculator.calculate(film.getType(), rental.getOverdue(now));
            }
        }

        List<Case> cases = new LinkedList<>();
        cases.add(new Case("old1", new Rental(1, customer, inventoryOld1, now.minusDays(2), now.plusDays(1))));
        cases.add(new Case("old2", new Rental(2, customer, inventoryOld2, now.minusDays(10), now.minusDays(3))));
        cases.add(new Case("new", new Rental(3, customer, inventoryNew, now.minusDays(10), now.minusDays(3))));

        UriBuilder builder = RESOURCES.client().target("/customers/" + customer.getId() + "/rentals")
                .getUriBuilder();
        Set<Rental> rentals = new HashSet<>();
        for (Case c : cases) {
            rentals.add(c.rental);
            when(RENTAL_DAO.findByInventory(eq(c.inventory))).thenReturn(c.rental);
            builder.queryParam("inventory_id", c.inventory.getId());
        }
        builder.queryParam("end", now.getMillis());
        CustomerRentalDeleteResult result = RESOURCES.client().target(builder)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .delete(CustomerRentalDeleteResult.class);

        assertEquals(customer.getId(), result.customerId);
        assertEquals(cases.size(), result.getItems().size());
        int numOverdue = 0;
        long fine = 0;
        for (int i = 0; i < cases.size(); i++) {
            Case c = cases.get(i);
            CustomerRentalDeleteResult.Item item = result.getItems().get(i);

            assertEquals(c.label, c.inventory.getId(), item.inventoryId);
            assertEquals(c.label, c.rental.getStart().getMillis(), item.start.getMillis());
            assertEquals(c.label, c.rental.getDue().getMillis(), item.due.getMillis());
            assertEquals(c.label, c.rental.getDuration().getDays(), item.duration);
            assertEquals(c.label, c.fine, item.fine);
            assertEquals(c.label, c.rental.getOverdue(item.end).getDays(), item.overdue);
            fine += c.fine;
            if (c.rental.getOverdue(item.end).getDays() > 0) numOverdue++;
        }
        assertEquals(numOverdue, result.numOverdue);
        assertEquals(fine, result.fine);
    }
}
