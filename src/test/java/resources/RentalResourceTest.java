package resources;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import dao.*;
import data.*;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import org.mockito.Captor;
import org.mockito.runners.MockitoJUnitRunner;
import results.RentalResult;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.*;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class RentalResourceTest {
    private static final RentalDAO RENTAL_DAO = mock(RentalDAO.class);
    private static final InventoryDAO INVENTORY_DAO = mock(InventoryDAO.class);
    private static final FilmDAO FILM_DAO = mock(FilmDAO.class);
    private static final CustomerDAO CUSTOMER_DAO = mock(CustomerDAO.class);
    private static final PriceDAO PRICE_DAO = mock(PriceDAO.class);

    @ClassRule
    public static final ResourceTestRule RESOURCES = ResourceTestRule.builder()
            .addResource(new RentalResource(RENTAL_DAO, INVENTORY_DAO, PRICE_DAO, CUSTOMER_DAO))
            .build();

    @Captor
    private ArgumentCaptor<Rental> rentalCaptor;
    private Rental rental;
    private Film filmNew, filmOld;
    private Inventory inventoryNew, inventoryOld;
    private Customer customer;

    @Before
    public void setUp() {
        Price basicPrice = new Price(Price.Type.BASIC, 30, Currency.SEK);
        Price premiumPrice = new Price(Price.Type.PREMIUM, 40, Currency.SEK);
        when(PRICE_DAO.findByType(Price.Type.BASIC)).thenReturn(basicPrice);
        when(PRICE_DAO.findByType(Price.Type.PREMIUM)).thenReturn(premiumPrice);
        customer = new Customer(1, "Killian", 10);
        filmNew = new Film(2,"The new", Film.Type.NEW);
        filmOld = new Film(3,"The old", Film.Type.OLD);
        inventoryNew = new Inventory(4, filmNew);
        inventoryOld = new Inventory(5, filmOld);
        rental = new Rental(customer, inventoryNew, new DateTime(), new DateTime().withDurationAdded(4000, 0));
    }

    @After
    public void tearDown() {
        reset(RENTAL_DAO);
        reset(PRICE_DAO);
        reset(FILM_DAO);
        reset(INVENTORY_DAO);
    }

    @Test
    public void createRental() {
        when(RENTAL_DAO.create(eq(customer), eq(inventoryNew), any(DateTime.class), any(DateTime.class))).thenReturn(rental);
        when(CUSTOMER_DAO.findById(any(Long.class))).thenReturn(customer);
        when(INVENTORY_DAO.findById(eq(inventoryNew.getId()))).thenReturn(inventoryNew);
        when(INVENTORY_DAO.findById(eq(inventoryOld.getId()))).thenReturn(inventoryOld);

        Form form = new Form()
                .param("customer_id", "" + customer.getId())
                .param("inventory_id", "" + inventoryNew.getId())
                .param("start", "" + rental.getStart().getMillis())
                .param("duration", "" + rental.getDuration().getDays());

        List<RentalResult> response = RESOURCES.client().target("/rentals")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE),
                        new GenericType<List<RentalResult>>() {});
        assertEquals(customer.getId(), response.get(0).customerId);
        assertEquals(inventoryNew.getId(), response.get(0).inventoryId);
    }
}

