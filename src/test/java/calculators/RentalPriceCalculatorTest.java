package calculators;

import data.Film;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class RentalPriceCalculatorTest {
    public static long basicPrice = 30;
    public static long premiumPrice = 40;

    @Test
    public void newBelow() {
        assertEquals(40, new RentalPriceCalculator(basicPrice, premiumPrice)
                .calculate(Film.Type.NEW, Period.days(1)));
    }

    @Test
    public void regBelow() {
        assertEquals(30, new RentalPriceCalculator(basicPrice, premiumPrice)
                .calculate(Film.Type.REGULAR, Period.days(2)));
    }

    @Test
    public void regAbove() {
        /*
        assertEquals(90, new RentalPriceCalculator(basicPrice, premiumPrice)
                .calculate(Film.Type.REGULAR, Period.days(5)));
                */
    }

    @Test
    public void oldAbove() {
        /*
        assertEquals(90, new RentalPriceCalculator(basicPrice, premiumPrice)
                .calculate(Film.Type.REGULAR, Period.days(7)));
                */
    }
}
