import data.Film;

import data.Rental;
import io.dropwizard.testing.ResourceHelpers;

import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.*;
import results.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class IntegrationTest {
    private static final String TMP_FILE = createTempFile();
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("test-config.yml");

    @ClassRule
    public static final DropwizardAppRule<CasumoConfig> RULE = new DropwizardAppRule<>(
            App.class, CONFIG_PATH,
            ConfigOverride.config("database.url", "jdbc:h2:" + TMP_FILE));

    private Client client;

    @Before
    public void setUp() throws Exception {
        client = ClientBuilder.newClient();
    }

    @After
    public void tearDown() throws Exception {
        client.close();
    }

    private static String createTempFile() {
        try {
            return File.createTempFile("test-example", null).getAbsolutePath();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Test
    public void testWorkflow() {
        final String url = "http://localhost:" + RULE.getLocalPort() + "/application/";

        //Create Customer
        CustomerResult customerResult = client.target(url + "customers")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new Form().param("name", "Killian"), MediaType.APPLICATION_FORM_URLENCODED_TYPE),
                        CustomerResult.class);
        assertEquals("Killian", customerResult.name);
        assertEquals(0, customerResult.points);

        //Create films
        FilmCreateResult filmsResult = client.target(url + "films")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(
                        new Form()
                                .param("title", "Matrix 11").param("type", "New")
                                .param("title", "Spiderman").param("type", "Regular")
                                .param("title", "Spiderman 2").param("type", "Regular")
                                .param("title", "Out of Africa").param("type", "Old"),
                        MediaType.APPLICATION_FORM_URLENCODED_TYPE),
                        FilmCreateResult.class);
        assertEquals(4, filmsResult.getCount());
        assertEquals(4, filmsResult.getItems().size());
        assertEquals("Matrix 11", filmsResult.getItems().get(0).title);
        assertEquals(Film.Type.NEW, filmsResult.getItems().get(0).type);
        assertEquals("Spiderman", filmsResult.getItems().get(1).title);
        assertEquals(Film.Type.REGULAR, filmsResult.getItems().get(1).type);

        //Create Inventory
        InventoryCreateResult inventoryResult = client.target(url + "inventory")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(
                        new Form()
                                .param("film_id", Long.toString(filmsResult.getItems().get(0).id))
                                .param("film_id", Long.toString(filmsResult.getItems().get(1).id))
                                .param("film_id", Long.toString(filmsResult.getItems().get(2).id))
                                .param("film_id", Long.toString(filmsResult.getItems().get(3).id))
                                .param("film_id", Long.toString(filmsResult.getItems().get(3).id)),
                        MediaType.APPLICATION_FORM_URLENCODED_TYPE),
                        InventoryCreateResult.class);
        List<InventoryResult> inventoryItems = inventoryResult.getItems();
        assertEquals(inventoryItems.get(0).filmId, filmsResult.getItems().get(0).id);

        //Get price information
        PriceListResult priceResult = client.target(url + "prices")
                .queryParam("inventory_id", inventoryItems.get(0).id)
                .queryParam("inventory_id", inventoryItems.get(1).id)
                .queryParam("inventory_id", inventoryItems.get(2).id)
                .queryParam("inventory_id", inventoryItems.get(3).id)
                .queryParam("inventory_id", inventoryItems.get(4).id)
                .queryParam("duration", Integer.toString(3))
                .queryParam("type", "RENTAL")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(PriceListResult.class);
        List<PriceListResult.Item> priceItems = priceResult.getItems();
        //assertEquals(priceResult.get(0).price, );
        assertEquals(2, priceItems.get(0).points);

        //Rent videos
        CustomerRentalCreateResult rentalsResult = client.target(url + "customers/" + customerResult.id + "/rentals")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new Form()
                                .param("inventory_id", "" + inventoryItems.get(0).id).param("duration", "" + 2)
                                .param("inventory_id", "" + inventoryItems.get(1).id).param("duration", "" + 3)
                                .param("inventory_id", "" + inventoryItems.get(2).id).param("duration", "" + 4),
                        MediaType.APPLICATION_FORM_URLENCODED_TYPE),
                        CustomerRentalCreateResult.class);
        List<CustomerRentalCreateResult.Item> rentalItems = rentalsResult.getItems();
        assertEquals(3, rentalItems.size());

        //Get rentals
        CustomerRentalListResult actualRentalsResult = client.target(url + "customers/" + customerResult.id + "/rentals")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(CustomerRentalListResult.class);
        List<CustomerRentalListResult.Item> actualRentalsItems = actualRentalsResult.getItems();
        assertEquals(rentalItems.size(), actualRentalsItems.size());

        //Return/delete rentals
        CustomerRentalDeleteResult returnedResult = client.target(url + "customers/" + customerResult.id + "/rentals")
                .queryParam("inventory_id", inventoryItems.get(0).id, inventoryItems.get(1).id)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .delete(CustomerRentalDeleteResult.class);
        List<CustomerRentalDeleteResult.Item> returnedItems = returnedResult.getItems();
        assertEquals(inventoryItems.get(0).id, returnedItems.get(0).inventoryId);
        assertEquals(inventoryItems.get(1).id, returnedItems.get(1).inventoryId);

        //Ensure correct number of rentals left
        actualRentalsResult = client.target(url + "customers/" + customerResult.id + "/rentals")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(CustomerRentalListResult.class);
        assertEquals(rentalItems.size() - returnedItems.size(), actualRentalsResult.getItems().size());
    }
}
