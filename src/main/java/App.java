import dao.*;
import data.*;
import resources.*;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;


public class App extends Application<CasumoConfig> {

    @Override
    public String getName() {
        return "App";
    }

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    private final HibernateBundle<CasumoConfig> hibernate = new HibernateBundle<CasumoConfig>(
            Film.class, Customer.class, Inventory.class, Rental.class, Price.class) {
        @Override
        public DataSourceFactory getDataSourceFactory(CasumoConfig configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    @Override
    public void initialize(Bootstrap<CasumoConfig> bootstrap) {
        bootstrap.addBundle(hibernate);
    }

    @Override
    public void run(CasumoConfig config, Environment environment) {
        //environment.getObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        PriceDAO priceDAO = new PriceDAO(hibernate.getSessionFactory());
        RentalDAO rentalDAO = new RentalDAO(hibernate.getSessionFactory());
        InventoryDAO inventoryDAO = new InventoryDAO(hibernate.getSessionFactory());
        CustomerDAO customerDAO = new CustomerDAO(hibernate.getSessionFactory());
        FilmDAO filmDAO = new FilmDAO(hibernate.getSessionFactory());

        environment.jersey().register(new FilmResource(filmDAO, priceDAO));
        environment.jersey().register(new RentalResource(rentalDAO, inventoryDAO, priceDAO, customerDAO));
        environment.jersey().register(new CustomerResource(customerDAO, rentalDAO, inventoryDAO, priceDAO));
        environment.jersey().register(new InventoryResource(inventoryDAO, filmDAO));
        environment.jersey().register(new PriceResource(priceDAO, filmDAO, inventoryDAO));
    }
}
