package dao;

import data.Customer;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;


public class CustomerDAO extends AbstractDAO<Customer> {

    public CustomerDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Customer findById(Long id) {
        return get(id);
    }

    public Customer create(String name) {
        return persist(new Customer(name));
    }
}

