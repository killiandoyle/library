package dao;

import data.Currency;
import data.Price;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;


public class PriceDAO extends AbstractDAO<Price> {

    public PriceDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Price findByType(Price.Type type) {
        Criteria criteria = currentSession().createCriteria(Price.class);
        criteria.add(Restrictions.eq("type", type));
        return uniqueResult(criteria);
    }

    public Price create(Price.Type type, long amount, Currency currency) {
        return persist(new Price(type, amount, currency));
    }
}
