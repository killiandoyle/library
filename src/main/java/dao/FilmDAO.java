package dao;

import io.dropwizard.hibernate.AbstractDAO;
import data.Film;
import org.hibernate.SessionFactory;


public class FilmDAO extends AbstractDAO<Film> {

    public FilmDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Film findById(Long id) {
        return get(id);
    }

    public Film create(String title, Film.Type type) {
        return persist(new Film(title, type));
    }
}
