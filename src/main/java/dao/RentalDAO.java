package dao;

import data.Customer;
import data.Inventory;
import data.Rental;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import javax.validation.constraints.NotNull;


public class RentalDAO extends AbstractDAO<Rental> {

    public RentalDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Rental findById(Long id) {
        return get(id);
    }

    public Rental findByInventory(@NotNull Inventory inventory) {
        Criteria criteria = currentSession().createCriteria(Rental.class);
        criteria.add(Restrictions.eq("inventory", inventory));
        return uniqueResult(criteria);
    }

    public void delete(Rental rental) {
        currentSession().delete(rental);
    }

    public Rental create(Customer customer, Inventory inventory, DateTime start, DateTime due) {
        return persist(new Rental(customer, inventory, start, due));
    }
}
