package dao;

import data.Film;
import data.Inventory;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;


public class InventoryDAO extends AbstractDAO<Inventory> {

    public InventoryDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Inventory findById(Long id) {
        return get(id);
    }

    public Inventory create(Film film) {
        return persist(new Inventory(film));
    }
}
