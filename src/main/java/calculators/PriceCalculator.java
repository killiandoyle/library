package calculators;

import data.Film;
import org.joda.time.Period;


public interface PriceCalculator {

    public long calculate(Film.Type type, Period period);
}
