package calculators;

import data.Film;
import org.joda.time.Period;


public class OverdueFineCalculator implements PriceCalculator {
    long basic, premium;

    public OverdueFineCalculator(long basic, long premium) {
        this.basic = basic;
        this.premium = premium;
    }

    public long calculate(Film.Type filmType, Period period) {
        if (period.getDays() < 0) return 0;
        return (filmType == Film.Type.NEW) ?
                premium * period.getDays() :
                basic * period.getDays();
    }
}
