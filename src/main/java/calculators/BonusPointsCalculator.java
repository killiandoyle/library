package calculators;

import data.Film;
import data.Inventory;
import org.joda.time.Period;


public class BonusPointsCalculator implements PriceCalculator {
    private long pointsForNew = 2;
    private long pointsForOther = 1;

    public BonusPointsCalculator() {}

    public long calculate(Film.Type filmType, Period period) {
        return (filmType == Film.Type.NEW) ? pointsForNew : pointsForOther;
    }
}
