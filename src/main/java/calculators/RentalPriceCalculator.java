package calculators;

import data.Film;
import org.joda.time.Period;


public class RentalPriceCalculator implements PriceCalculator {
    long basic, premium;

    public RentalPriceCalculator(long basic, long premium) {
        this.basic = basic;
        this.premium = premium;
    }

    public long calculate(Film.Type filmType, Period period) {
        int days = period.getDays();
        switch (filmType) {
            case NEW:
                return premium * days;
            case REGULAR:
                return days <= 3 ? basic : basic * days / 3;
            case OLD:
                return days <= 5 ? basic : basic * days / 5;
        }
        throw new RuntimeException("Should not be here");
    }
}
