package results;

import com.fasterxml.jackson.annotation.JsonProperty;
import data.Customer;
import data.Rental;
import org.joda.time.DateTime;
import org.joda.time.Period;


public class CustomerRentalListResult extends AbstractCollectionResult<CustomerRentalListResult.Item> {
    public static class Item {
        @JsonProperty("inventory_id")
        public long inventoryId;
        public DateTime start, due, now;
        public long duration, overdue;
        public long fine;

        public Item() {
            this.fine = 0;
        }

        public Item(Rental rental, DateTime now, long fine) {
            this();
            this.inventoryId = rental.getInventory().getId();
            this.start = rental.getStart();
            this.due = rental.getDue();
            this.now = now;
            this.duration = new Period(start, due).getDays();
            this.overdue = new Period(due, now).getDays();
            this.fine = fine;
        }
    }

    @JsonProperty("customer_id")
    public long customerId;
    @JsonProperty("num_overdue")
    public int numOverdue;
    public long fine;

    public CustomerRentalListResult() {
        super();
        this.fine = 0;
        this.numOverdue = 0;
    }

    public CustomerRentalListResult(Customer customer) {
        this();
        this.customerId = customer.getId();
    }
}
