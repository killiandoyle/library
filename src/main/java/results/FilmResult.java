package results;

import data.Film;

public class FilmResult {
    public long id;
    public String title;
    public Film.Type type;

    public FilmResult() {}

    public FilmResult(Film film) {
        this.id = film.getId();
        this.title = film.getTitle();
        this.type = film.getType();
    }
}
