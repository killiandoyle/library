package results;

import com.fasterxml.jackson.annotation.JsonProperty;
import data.Customer;
import data.Rental;
import org.joda.time.DateTime;
import org.joda.time.Period;


public class CustomerRentalDeleteResult extends AbstractCollectionResult<CustomerRentalDeleteResult.Item> {
    public static class Item {
        @JsonProperty("inventory_id")
        public long inventoryId;
        public DateTime start, due, end;
        public long duration, overdue;
        public long fine;

        public Item() {}

        public Item(Rental rental, DateTime end, long fine) {
            this();
            this.inventoryId = rental.getInventory().getId();
            this.start = rental.getStart();
            this.due = rental.getDue();
            this.end = end;
            this.duration = new Period(start, due).getDays();
            this.overdue = new Period(due, end).getDays();
            this.fine = fine;
        }
    }

    @JsonProperty("customer_id")
    public long customerId;
    public long fine;
    @JsonProperty("num_overdue")
    public int numOverdue;

    public CustomerRentalDeleteResult() {
        super();
        this.fine = 0;
        this.numOverdue = 0;
    }

    public CustomerRentalDeleteResult(Customer customer) {
        this();
        this.customerId = customer.getId();
    }
}
