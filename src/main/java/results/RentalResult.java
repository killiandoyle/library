package results;

import com.fasterxml.jackson.annotation.JsonProperty;
import data.Rental;
import org.joda.time.DateTime;

public class RentalResult {
    public long id;
    @JsonProperty("inventory_id")
    public long inventoryId;
    @JsonProperty("customer_id")
    public long customerId;
    public DateTime start, due;

    public RentalResult() {}

    public RentalResult(Rental rental) {
        this.id = rental.getId();
        this.inventoryId = rental.getInventory().getId();
        this.customerId = rental.getCustomer().getId();
        this.start = rental.getStart();
        this.due = rental.getDue();
    }
}
