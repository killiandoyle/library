package results;

import com.fasterxml.jackson.annotation.JsonProperty;
import data.Inventory;

public class InventoryResult {
    public long id;
    @JsonProperty("film_id")
    public long filmId;

    public InventoryResult() {}

    public InventoryResult(Inventory inventory) {
        this.id = inventory.getId();
        this.filmId = inventory.getFilm().getId();
    }
}
