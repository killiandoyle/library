package results;

import data.Customer;

public class CustomerResult {
    public long id;
    public String name;
    public int points;

    public CustomerResult() {}

    public CustomerResult(Customer customer) {
        this.id = customer.getId();
        this.name = customer.getName();
        this.points = customer.getPoints();
    }
}