package results;


import com.fasterxml.jackson.annotation.JsonProperty;

public class PriceListResult extends AbstractCollectionResult<PriceListResult.Item> {
    public static class Item {
        @JsonProperty("inventory_id")
        public long inventoryId;
        public long price, points;

        public Item() {}

        public Item(long inventoryIid, long price, long points) {
            this.inventoryId = inventoryId;
            this.price = price;
            this.points = points;
        }
    }

    public long price, points;

    public PriceListResult() {
        super();
        this.price = 0;
        this.points = 0;
    }
}