package results;

import com.fasterxml.jackson.annotation.JsonProperty;
import data.Customer;
import data.Rental;
import org.joda.time.DateTime;
import org.joda.time.Period;


public class CustomerRentalCreateResult extends AbstractCollectionResult<CustomerRentalCreateResult.Item> {
    public static class Item {
        @JsonProperty("inventory_id")
        public long inventoryId;
        public long price, points;
        public DateTime start, due;
        public long duration;

        public Item() {}

        public Item(Rental rental, long price, long points) {
            this();
            this.inventoryId = rental.getInventory().getId();
            this.start = rental.getStart();
            this.due = rental.getDue();
            this.duration = new Period(start, due).getDays();
            this.price = price;
            this.points = points;
        }
    }

    @JsonProperty("customer_id")
    public long customerId;
    public long price, points;

    public CustomerRentalCreateResult() {
        super();
        this.price = 0;
        this.points = 0;
    }

    public CustomerRentalCreateResult(Customer customer) {
        this();
        this.customerId = customer.getId();
    }
}
