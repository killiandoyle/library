package results;

import java.util.LinkedList;
import java.util.List;


public abstract class AbstractCollectionResult<T> {

    protected List<T> items;

    public AbstractCollectionResult() {
        items = new LinkedList<>();
    }

    public int getCount() {
        return items.size();
    }

    public void setCount(int count) {}

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public boolean add(T item) {
        return this.items.add(item);
    }
}
