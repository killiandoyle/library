package data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "Customers")
public class Customer {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "POINTS", nullable = false)
    private int points;

    public Customer() {
        this.points = 0;
    }

    public Customer(String name) {
        this();
        this.name = name;
    }

    public Customer(long id, String name, int points) {
        this.id = id;
        this.name = name;
        this.points = points;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public long addPoints(long somePoints) {
        this.points += somePoints;
        return this.points;
    }

    @OneToMany(targetEntity = Rental.class, mappedBy = "customer", cascade = CascadeType.ALL)
    Set<Rental> rentals = new HashSet<>();

    public Set<Rental> getRentals() {
        return rentals;
    }
}
