package data;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;


@Entity
@Table(name = "Inventory")
public class Inventory {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    @JsonProperty
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILM_ID", nullable = false)
    private Film film;

    public Inventory() {}

    public Inventory(Film film) {
        this.film = film;
    }

    public Inventory(long id, Film film) {
        this(film);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Film getFilm() {
        return film;
    }
}
