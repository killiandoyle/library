package data;

import javax.persistence.*;


@Entity
@Table(name = "Prices")
public class Price {
    public enum Type {
        BASIC, PREMIUM
    }

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "Type", unique = true, nullable = false)
    private Type type;

    @Column(name = "Amount", nullable = false)
    private long amount;

    @Column(name = "Currency", nullable = false)
    @Enumerated(EnumType.STRING)
    private Currency currency;

    public Price() {}

    public Price(Type type, long amount, Currency currency) {
        this.type = type;
        this.amount = amount;
        this.currency = currency;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public Price.Type getType() { return type; }
}
