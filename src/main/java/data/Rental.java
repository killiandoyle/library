package data;

import org.joda.time.DateTime;
import org.joda.time.Period;

import javax.persistence.*;


@Entity
@Table(name = "Rentals", indexes = {
        @Index(columnList = "INVENTORY_ID", name = "rentals_inventory_idx")
})
public class Rental {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID", nullable = false)
    private Customer customer;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVENTORY_ID", nullable = false, unique = true)
    private Inventory inventory;

    @Column(name = "START", nullable = false)
    private DateTime start;

    @Column(name = "DUE", nullable = false)
    private DateTime due;

    public Rental() {}

    public Rental(Customer customer, Inventory inventory, DateTime start, DateTime due) {
        this.customer = customer;
        this.inventory = inventory;
        this.start = start;
        this.due = due;
    }

    public Rental(long id, Customer customer, Inventory inventory, DateTime start, DateTime due) {
        this(customer, inventory, start, due);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public DateTime getDue() {
        return due;
    }

    public DateTime getStart() {
        return start;
    }

    public Period getDuration() {
        return new Period(start, due);
    }

    public Period getOverdue(DateTime time) {
        return new Period(due, new DateTime(time));
    }
}
