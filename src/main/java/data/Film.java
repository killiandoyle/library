package data;

import javax.persistence.*;


@Entity
@Table(name = "Films")
public class Film {
    public enum Type {
        NEW, REGULAR, OLD
    }

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "TITLE", nullable = false, length = 4096)
    private String title;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE", nullable = false)
    private Type type;

    public Film() {}

    public Film(String title, Film.Type type) {
        this.title = title;
        this.type = type;
    }

    public Film(long id, String title, Film.Type type) {
        this(title, type);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Type getType() {
        return type;
    }
}
