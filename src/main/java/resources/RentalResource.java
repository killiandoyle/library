package resources;

import com.codahale.metrics.annotation.Timed;
import dao.CustomerDAO;
import dao.InventoryDAO;
import dao.PriceDAO;
import dao.RentalDAO;
import data.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import results.RentalResult;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;


@Path("/rentals")
@Produces(MediaType.APPLICATION_JSON)
public class RentalResource {
    private RentalDAO rentalDAO;
    private InventoryDAO inventoryDAO;
    private PriceDAO priceDAO;
    private CustomerDAO customerDAO;


    public RentalResource(RentalDAO rentalDAO, InventoryDAO inventoryDAO, PriceDAO priceDAO, CustomerDAO customerDAO) {
        this.rentalDAO = rentalDAO;
        this.inventoryDAO = inventoryDAO;
        this.priceDAO = priceDAO;
        this.customerDAO = customerDAO;
    }

    @POST @Timed @UnitOfWork
    @Consumes("application/x-www-form-urlencoded")
    public List<RentalResult> create(@FormParam("customer_id") @NotNull Long customerId,
                         @FormParam("inventory_id") @NotNull List<Long> inventoryIds,
                         @FormParam("start") @NotNull Long startRaw,
                         @FormParam("duration") @NotNull int duration) {

        Customer customer = customerDAO.findById(customerId);
        DateTime start = new DateTime(startRaw);
        DateTime due = start.plusDays(duration);
        if (customer == null) throw new WebApplicationException("Customer not found", Response.Status.NOT_FOUND);
        List<RentalResult> result = new LinkedList<>();
        for (long inventoryId : inventoryIds) {
            Inventory inventory = inventoryDAO.findById(inventoryId);
            if (inventory == null) throw new WebApplicationException("Inventory id not found", Response.Status.NOT_FOUND);
            if (rentalDAO.findByInventory(inventory) != null)
                throw new WebApplicationException("Inventory item already rented", Response.Status.CONFLICT);
            Rental rental = rentalDAO.create(customer, inventory, start, due);
            result.add(new RentalResult(rental));
        }
        return result;
    }

    @GET @Timed @UnitOfWork
    public List<RentalResult> get(@QueryParam("inventory_id") List<Long> inventoryIds) {
        List<RentalResult> result = new LinkedList<>();
        for (long inventoryId : inventoryIds) {
            Inventory inventory = inventoryDAO.findById(inventoryId);
            if (inventory == null) throw new WebApplicationException("Inventory not found", Response.Status.NOT_FOUND);
            Rental rental = rentalDAO.findByInventory(inventory);
            if (rental == null) throw new WebApplicationException("Inventory not found", Response.Status.NOT_FOUND);
            result.add(new RentalResult(rental));
        }
        return result;
    }
}
