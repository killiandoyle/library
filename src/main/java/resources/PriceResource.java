package resources;

import com.codahale.metrics.annotation.Timed;
import dao.FilmDAO;
import dao.InventoryDAO;
import dao.PriceDAO;
import data.Film;
import data.Inventory;
import data.Price;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.Period;
import results.PriceListResult;
import calculators.OverdueFineCalculator;
import calculators.BonusPointsCalculator;
import calculators.RentalPriceCalculator;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;


@Path("/prices")
@Produces(MediaType.APPLICATION_JSON)
public class PriceResource {
    PriceDAO priceDao;
    FilmDAO filmDAO;
    InventoryDAO inventoryDAO;

    public PriceResource(PriceDAO priceDAO, FilmDAO filmDAO, InventoryDAO inventoryDAO) {
        this.priceDao = priceDAO;
        this.filmDAO = filmDAO;
        this.inventoryDAO = inventoryDAO;
    }

    @GET
    @Timed
    @UnitOfWork
    public PriceListResult get(@QueryParam("inventory_id") List<Long> inventoryIds,
                               @QueryParam("duration") @NotNull int duration,
                               @QueryParam("type") @NotNull @Pattern(regexp = "OVERDUE|RENTAL") String type) {

        PriceListResult result = new PriceListResult();
        RentalPriceCalculator priceCalculator = new RentalPriceCalculator(
                priceDao.findByType(Price.Type.BASIC).getAmount(),
                priceDao.findByType(Price.Type.PREMIUM).getAmount());
        OverdueFineCalculator fineCalculator = new OverdueFineCalculator(
                priceDao.findByType(Price.Type.BASIC).getAmount(),
                priceDao.findByType(Price.Type.PREMIUM).getAmount());
        BonusPointsCalculator pointsCalculator = new BonusPointsCalculator();
        for (long inventoryId : inventoryIds) {
            Inventory inventory = inventoryDAO.findById(inventoryId);
            if (inventory == null) throw new WebApplicationException("Inventory item not found", NOT_FOUND);
            long price, points = 0;
            Period period = Period.days(duration);
            Film.Type filmType = inventory.getFilm().getType();
            if (type.equals("RENTAL")) {
                points = pointsCalculator.calculate(filmType, period);
                price = priceCalculator.calculate(filmType, period);
                result.points += points;
            } else {
                price = fineCalculator.calculate(filmType, period);
                result.price += price;
            }
            result.add(new PriceListResult.Item(inventoryId, price, points));
        }
        return result;
    }
}
