package resources;

import com.codahale.metrics.annotation.Timed;
import dao.FilmDAO;
import dao.PriceDAO;
import data.Film;
import io.dropwizard.hibernate.UnitOfWork;
import results.FilmCreateResult;
import results.FilmResult;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/films")
@Produces(MediaType.APPLICATION_JSON)
public class FilmResource {
    FilmDAO filmDAO;
    PriceDAO priceDAO;

    public FilmResource(FilmDAO filmDAO, PriceDAO priceDAO) {
        this.filmDAO = filmDAO;
        this.priceDAO = priceDAO;
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes("application/x-www-form-urlencoded")
    public FilmCreateResult post(@FormParam("title") @NotNull List<String> titles,
                                 @FormParam("type") @NotNull List<String> types) {

        if (titles.size() != types.size())
            throw new WebApplicationException("must give type for each title", Response.Status.BAD_REQUEST);

        FilmCreateResult result = new FilmCreateResult();
        for (int i = 0; i < titles.size(); i++) {
            String title = titles.get(i), type = types.get(i).toUpperCase();
            Film.Type filmType;
            if (type == null || type.equals("NEW")) {
                filmType = Film.Type.NEW;
            } else if (type.equals("REGULAR")) {
                filmType = Film.Type.REGULAR;
            } else if (type.equals("OLD")) {
                filmType = Film.Type.OLD;
            } else {
                throw new WebApplicationException("Invalid type", Response.Status.BAD_REQUEST);
            }
            result.add(new FilmResult(filmDAO.create(title, filmType)));
        }
        return result;
    }
}
