package resources;

import com.codahale.metrics.annotation.Timed;
import dao.FilmDAO;
import dao.InventoryDAO;
import data.Film;
import data.Inventory;
import io.dropwizard.hibernate.UnitOfWork;
import results.InventoryCreateResult;
import results.InventoryResult;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;


@Path("/inventory")
@Produces(MediaType.APPLICATION_JSON)
public class InventoryResource {
    private InventoryDAO inventoryDAO;
    private FilmDAO filmDAO;

    public InventoryResource(InventoryDAO inventoryDAO, FilmDAO filmDAO) {
        this.inventoryDAO = inventoryDAO;
        this.filmDAO = filmDAO;
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes("application/x-www-form-urlencoded")
    public InventoryCreateResult create(@FormParam("film_id") @NotNull List<Long> filmIds) {
        InventoryCreateResult result = new InventoryCreateResult();
        for (long filmId : filmIds) {
            Film film = filmDAO.findById(filmId);
            if (film == null) throw new WebApplicationException("film not found", Response.Status.NOT_FOUND);
            result.add(new InventoryResult(inventoryDAO.create(film)));
        }
        return result;
    }

    @GET
    @Path("/{id}")
    @Timed
    @UnitOfWork
    public InventoryResult get(@PathParam("id") @NotNull long id) {
        final Inventory inventory = inventoryDAO.findById(id);
        if (inventory == null) throw new WebApplicationException("Could not find inventory", Response.Status.NOT_FOUND);
        return new InventoryResult(inventory);
    }
}
