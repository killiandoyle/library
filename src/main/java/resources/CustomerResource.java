package resources;

import com.codahale.metrics.annotation.Timed;
import dao.CustomerDAO;
import dao.InventoryDAO;
import dao.PriceDAO;
import dao.RentalDAO;
import data.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.Period;
import results.*;
import calculators.OverdueFineCalculator;
import calculators.BonusPointsCalculator;
import calculators.RentalPriceCalculator;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/customers")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerResource {
    private CustomerDAO customerDAO;
    private RentalDAO rentalDAO;
    private InventoryDAO inventoryDAO;
    private PriceDAO priceDAO;

    public CustomerResource(CustomerDAO customerDAO, RentalDAO rentalDao, InventoryDAO inventoryDAO, PriceDAO priceDAO) {
        this.customerDAO = customerDAO;
        this.rentalDAO = rentalDao;
        this.inventoryDAO = inventoryDAO;
        this.priceDAO = priceDAO;
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes("application/x-www-form-urlencoded")
    public CustomerResult post(@FormParam("name") @NotNull String name) {
        return new CustomerResult(customerDAO.create(name));
    }

    @GET
    @Path("/{id}")
    @Timed
    @UnitOfWork
    public CustomerResult post(@PathParam("id") @NotNull long id) {
        return new CustomerResult(customerDAO.findById(id));
    }

    @GET
    @Path("/{id}/rentals")
    @Timed
    @UnitOfWork
    public CustomerRentalListResult createRentals(@PathParam("id") long id) {
        Customer customer = customerDAO.findById(id);
        if (customer == null) throw new WebApplicationException("Customer not found", Response.Status.NOT_FOUND);
        CustomerRentalListResult result = new CustomerRentalListResult(customer);
        DateTime now = new DateTime();
        OverdueFineCalculator fineCalculator = new OverdueFineCalculator(
                priceDAO.findByType(Price.Type.BASIC).getAmount(),
                priceDAO.findByType(Price.Type.PREMIUM).getAmount());
        for (Rental rental : customer.getRentals()) {
            Inventory inventory = rental.getInventory();
            Film.Type filmType = inventory.getFilm().getType();
            Period overdue = new Period(rental.getDue(), now);
            long fine = fineCalculator.calculate(filmType, overdue);
            result.add(new CustomerRentalListResult.Item(rental, now, fine));
            if (overdue.getDays() > 0) result.numOverdue++;
            result.fine += fine;
        }
        return result;
    }

    @POST
    @Path("/{id}/rentals")
    @Timed
    @UnitOfWork
    @Consumes("application/x-www-form-urlencoded")
    public CustomerRentalCreateResult createRentals(@PathParam("id") long id,
                                                    @FormParam("start") Long rawStart,
                                                    @FormParam("inventory_id") @NotNull List<Long> inventoryIds,
                                                    @FormParam("duration") @NotNull List<Integer> durations) {
        if (inventoryIds.size() != durations.size())
            throw new WebApplicationException("inventory and durations don't match", Response.Status.BAD_REQUEST);
        DateTime start = new DateTime(rawStart);
        Customer customer = customerDAO.findById(id);
        if (customer == null) throw new WebApplicationException("Customer not found", Response.Status.NOT_FOUND);
        CustomerRentalCreateResult result = new CustomerRentalCreateResult(customer);
        RentalPriceCalculator priceCalculator = new RentalPriceCalculator(
                priceDAO.findByType(Price.Type.BASIC).getAmount(),
                priceDAO.findByType(Price.Type.PREMIUM).getAmount());
        BonusPointsCalculator pointsCalculator = new BonusPointsCalculator();
        for (int i = 0; i < inventoryIds.size(); i++) {
            long inventoryId = inventoryIds.get(i);
            int duration = durations.get(i);
            if (duration < 1) throw new WebApplicationException("Duration must be positive", Response.Status.BAD_REQUEST);
            Inventory inventory = inventoryDAO.findById(inventoryId);
            if (inventory == null) throw new WebApplicationException("Inventory not found", Response.Status.NOT_FOUND);
            Rental rental = rentalDAO.findByInventory(inventory);
            if (rental != null) throw new WebApplicationException("Already rented", Response.Status.CONFLICT);
            rental = rentalDAO.create(customer, inventory, start, start.plusDays(duration));
            Period period = Period.days(duration);
            long price = priceCalculator.calculate(inventory.getFilm().getType(), period);
            long points = pointsCalculator.calculate(inventory.getFilm().getType(), period);
            result.add(new CustomerRentalCreateResult.Item(rental, price, points));
            customer.addPoints(points);
            result.price += price;
            result.points += points;
        }
        return result;
    }

    @DELETE
    @Path("/{id}/rentals")
    @Timed
    @UnitOfWork
    public CustomerRentalDeleteResult deleteRental(@PathParam("id") long id,
                                                   @QueryParam("inventory_id") List<Long> inventoryIds,
                                                   @QueryParam("end") Long endRaw) {
        DateTime end = new DateTime(endRaw);
        Customer customer = customerDAO.findById(id);
        if (customer == null) throw new WebApplicationException("Customer not found", Response.Status.NOT_FOUND);
        CustomerRentalDeleteResult result = new CustomerRentalDeleteResult(customer);
        OverdueFineCalculator fineCalculator = new OverdueFineCalculator(priceDAO.findByType(Price.Type.BASIC).getAmount(),
                priceDAO.findByType(Price.Type.PREMIUM).getAmount());
        for (long inventoryId : inventoryIds) {
            Inventory inventory = inventoryDAO.findById(inventoryId);
            if (inventory == null) throw new WebApplicationException("Inventory not found", Response.Status.NOT_FOUND);
            Rental rental = rentalDAO.findByInventory(inventory);
            if (rental == null) throw new WebApplicationException("Not rented", Response.Status.NOT_FOUND);
            if (rental.getCustomer() != customer) throw new WebApplicationException("Rental belongs to someone else", Response.Status.FORBIDDEN);
            long fine = fineCalculator.calculate(inventory.getFilm().getType(), rental.getOverdue(end));
            result.add(new CustomerRentalDeleteResult.Item(rental, end, fine));
            result.fine += fine;
            if (fine > 0) result.numOverdue++;
            rentalDAO.delete(rental);
        }
        return result;
    }
}
